#!/bin/bash
set -euo pipefail

# Make a note of the package versions.
buildah --version

# Use vfs with buildah. Docker offers overlayfs as a default, but buildah
# cannot stack overlayfs on top of another overlayfs filesystem.
export STORAGE_DRIVER=vfs

# Write all image metadata in the docker format, not the standard OCI format.
# Newer versions of docker can handle the OCI format, but older versions, like
# the one shipped with Fedora 30, cannot handle the format.
export BUILDAH_FORMAT=docker

function say { echo "$@" | toilet -f mono12 -w 400 | lolcat -f || true; }
function echo_green { echo -e "\e[1;32m${1}\e[0m"; }
function echo_red { echo -e "\e[1;31m${1}\e[0m"; }
function echo_yellow { echo -e "\e[1;33m${1}\e[0m"; }

declare -i FAILED=0

function loop {
    _loop 5 10 only_notify "$@"
}

function loop_or_fail {
    _loop 5 10 exit_on_fail "$@"
}

function _loop {
    local count=$1 delay=$2 exit_on_fail=$3 s
    shift 3
    for i in $(seq "$count"); do
        # we want set -e still in effect, but also get the exit code; so
        # use a background process, as set -e is kept for background
        # commands, and the exit code checking is possible with wait later
        "$@" &
        wait $! && s=0 && break || s=$? && sleep "$delay"
    done
    FAILED+=$((s > 0))
    if [ "$exit_on_fail" = "exit_on_fail" ]; then
        (exit $s)
    fi
}

# Build the container.
say "$IMAGE_NAME"

# Determine whether/how to push the image:
# - if a tag was provided, use it for the image
# - if this is an MR pipeline, use mr-1234
# - if we're on the default branch, use 'latest'
# - otherwise, do not push
if [ -v CI_COMMIT_TAG ]; then
    IMAGE_TAG=${CI_COMMIT_TAG}
    IMAGE_DESCRIPTION="${CI_PROJECT_PATH}/${IMAGE_NAME}:${IMAGE_TAG}@git-${CI_COMMIT_SHORT_SHA}"
    IMAGE_PUSH=1
elif [ -v CI_MERGE_REQUEST_IID ]; then
    IMAGE_TAG=mr-${CI_MERGE_REQUEST_IID}
    IMAGE_DESCRIPTION="${CI_MERGE_REQUEST_SOURCE_PROJECT_PATH}:${IMAGE_TAG}@git-${CI_COMMIT_SHORT_SHA}"
    IMAGE_PUSH=1
elif [ -v CI_COMMIT_REF_NAME ] && [ "${CI_COMMIT_REF_NAME}" = "${CI_DEFAULT_BRANCH}" ]; then
    IMAGE_TAG="latest"
    IMAGE_DESCRIPTION="${CI_PROJECT_PATH}/${IMAGE_NAME}:${IMAGE_TAG}@git-${CI_COMMIT_SHORT_SHA}"
    IMAGE_PUSH=1
else
    if ! [ -v IMAGE_TAG ]; then
        IMAGE_TAG="latest"
    fi
    IMAGE_DESCRIPTION="Local build for ${IMAGE_NAME}:${IMAGE_TAG}"
fi

export CPATH=includes:/usr/local/share/cki

if ! [ -v OPENSHIFT_REGISTRY ]; then
    # build and push via buildah

    echo_yellow "Buildah build"

    # enable qemu support; this works in the buildah image, and should cause no
    # harm on a local installation where the script most likely is not installed
    if [ -n "${IMAGE_ARCH:-}" ] && [ -x /usr/local/bin/qemu-binfmt-conf.sh ]; then
        qemu-binfmt-conf.sh --qemu-suffix -static --qemu-path /usr/bin --persistent yes || true
    fi

    IMAGE="${IMAGE_NAME}:${IMAGE_TAG}${IMAGE_ARCH:+-${IMAGE_ARCH}}"

    if ! [ -v IMAGE_ARCHES ]; then
        loop_or_fail buildah bud \
            ${base_image+--build-arg "BASE_IMAGE=$base_image"} \
            ${base_image_tag+--build-arg "BASE_IMAGE_TAG=$base_image_tag"} \
            ${IMAGE_DESCRIPTION+--build-arg "IMAGE_DESCRIPTION=$IMAGE_DESCRIPTION"} \
            ${IMAGE_ARCH:+--arch "$IMAGE_ARCH"} \
            ${IMAGE_ARCH:+--override-arch "$IMAGE_ARCH"} \
            --file "builds/${IMAGE_NAME}"* \
            --tag "${IMAGE}" \
            .
    fi

    if ! [ -v IMAGE_PUSH ]; then
        exit 0
    fi

    REGISTRIES=(CI_REGISTRY QUAY_IO DOCKER_IO)

    export REGISTRY_AUTH_FILE=${HOME}/auth.json
    for REGISTRY_VAR in "${REGISTRIES[@]}"; do
        if ! [ -v "${REGISTRY_VAR}" ]; then
            continue
        fi
        REGISTRY_PASSWORD_VAR="${REGISTRY_VAR}_PASSWORD"
        REGISTRY_USER_VAR="${REGISTRY_VAR}_USER"
        REGISTRY_IMAGE_VAR="${REGISTRY_VAR}_IMAGE"
        echo "${!REGISTRY_PASSWORD_VAR}" | buildah login -u "${!REGISTRY_USER_VAR}" --password-stdin "${!REGISTRY_VAR}"
        REGISTRY_TAG=docker://${!REGISTRY_IMAGE_VAR}/${IMAGE}

        if [ -v IMAGE_ARCHES ]; then
            IFS=' ' read -r -a IMAGE_ARCHES_ARRAY <<< "${IMAGE_ARCHES}"
            buildah rmi "${IMAGE}" > /dev/null 2>&1 || true
            for IMAGE_ARCH in "${IMAGE_ARCHES_ARRAY[@]}"; do
                echo_yellow "pulling ${REGISTRY_TAG}-$IMAGE_ARCH"
                # quay.io downgrades manifests on upload, but then refuses to
                # accept them in list manifests; upgrade them locally
                skopeo copy "${REGISTRY_TAG}-$IMAGE_ARCH" "containers-storage:${!REGISTRY_IMAGE_VAR}/${IMAGE}-${IMAGE_ARCH}" --format v2s2
            done
            buildah manifest create "${IMAGE}" "${IMAGE_ARCHES_ARRAY[@]/#/${REGISTRY_TAG}-}"
            echo_yellow "pushing ${REGISTRY_TAG}"
            loop buildah manifest push --all "${IMAGE}" "${REGISTRY_TAG}"
        else
            echo_yellow "pushing ${REGISTRY_TAG}"
            loop buildah push "${IMAGE}" "${REGISTRY_TAG}"
        fi
    done

    if [ "${FAILED}" -gt 0 ]; then
        echo_red "$FAILED registry pushes failed."
        exit 1
    fi
else
    # build and push via OpenShift BuildConfigs

    echo_yellow "OpenShift build"

    cpp -E -traditional -undef "builds/${IMAGE_NAME}"* > Dockerfile

    resource='{
        "apiVersion": "build.openshift.io/v1",
        "kind": "BuildConfig",
        "metadata": {
            "labels": {
            }
        },
        "spec": {
            "resources": {
                "limits": {
                    "cpu": "1"
                }
            },
            "source": {
                "type": "Binary"
            },
            "strategy": {
                "type": "Docker",
                "dockerStrategy": {
                    "imageOptimizationPolicy": "SkipLayers",
                    "buildArgs": []
                }
            }
        }
    }'

    for i in base_image-BASE_IMAGE base_image_tag-BASE_IMAGE_TAG IMAGE_DESCRIPTION-IMAGE_DESCRIPTION; do
        varname=${i%-*}
        if [ -v "$varname" ]; then
            resource=$(jq \
                --arg name "${i#*-}" \
                --arg value "${!varname}" \
                '.spec.strategy.dockerStrategy.buildArgs += [{"name": $name, "value": $value}]' \
                <<< "$resource")
        fi
    done

    if [ -v IMAGE_TAG ]; then
        resource=$(jq \
            --arg tag "${OPENSHIFT_REGISTRY}/cki/${IMAGE_NAME}:${IMAGE_TAG}" \
            --arg secret "${OPENSHIFT_PUSH_SECRET_NAME}" \
            '.spec.output = {"pushSecret": {"name": $secret}, "to": {"kind": "DockerImage", "name": $tag}}' \
            <<< "$resource")
    fi

    BUILD_ID="container-image-build-${IMAGE_NAME}-${CI_JOB_ID:-$(openssl rand -hex 4)}"
    resource=$(jq \
        --arg build_id "${BUILD_ID}" \
        '.metadata.labels.build = $build_id | .metadata.name = $build_id' \
        <<< "$resource")

    trap 'oc delete bc/${BUILD_ID} build/${BUILD_ID}-1' EXIT
    oc create -f - <<< "$resource"
    oc start-build --from-dir . "${BUILD_ID}"
    while true; do
        oc logs --follow --tail 10 "build/${BUILD_ID}-1" || true
        case "$(oc get "build/${BUILD_ID}-1" -o jsonpath='{.status.phase}')" in
            Complete)
                exit 0
                ;;
            Failed|Error|Cancelled)
                oc describe "build/${BUILD_ID}-1"
                exit 1
                ;;
        esac
        echo '...reconnecting...'
    done
fi
